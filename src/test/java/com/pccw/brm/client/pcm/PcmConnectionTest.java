package com.pccw.brm.client.pcm;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.mockito.InOrder;

import com.pccw.brm.client.TransactionHandler;
import com.portal.pcm.EBufException;
import com.portal.pcm.FList;
import com.portal.pcm.PortalContext;
import com.portal.pcm.PortalException;

public class PcmConnectionTest {
    @Test
    public void constructor_nullContext_throwIllegalArgument() {
        assertThrows(IllegalArgumentException.class, () -> new PcmConnection(null));
    }

    @Test
    public void close_callContextClose() throws PortalException {
        PortalContext mockContext = mock(PortalContext.class);
        PcmConnection testConnection = new PcmConnection(mockContext);

        testConnection.close();

        verify(mockContext).close(anyBoolean());
    }

    @Test
    public void close_subsequentCallsDoNothing() throws PortalException {
        PortalContext mockContext = mock(PortalContext.class);
        PcmConnection testConnection = new PcmConnection(mockContext);

        testConnection.close();
        testConnection.close();

        verify(mockContext).close(anyBoolean());
    }

    @Test
    public void close_contextIsConnected_callContextDisconnect() throws PortalException {
        PortalContext mockContext = mock(PortalContext.class);
        PcmConnection testConnection = new PcmConnection(mockContext);

        testConnection.execute(0, mock(FList.class));
        testConnection.close();

        verify(mockContext).disconnect();
    }

    @Test
    public void close_contextIsNotConnected_doNotcallContextDisconnect() throws PortalException {
        PortalContext mockContext = mock(PortalContext.class);
        PcmConnection testConnection = new PcmConnection(mockContext);

        testConnection.close();

        verify(mockContext, never()).disconnect();
    }

    @Test
    @SuppressWarnings("resource")
    public void execute_callContextExecute() throws PortalException {
        int opcode = 5;
        FList inputFlist = mock(FList.class);

        assertAll(
            () -> {
                PortalContext mockContext = mock(PortalContext.class);
                PcmConnection testConnection = new PcmConnection(mockContext);

                testConnection.execute(opcode, inputFlist);

                verify(mockContext).opcode(eq(opcode), anyInt(), same(inputFlist));
            },
            () -> {
                PortalContext mockContext = mock(PortalContext.class);
                PcmConnection testConnection = new PcmConnection(mockContext);

                int flags = 7;
                testConnection.execute(opcode, flags, inputFlist);

                verify(mockContext).opcode(eq(opcode), eq(flags), same(inputFlist));
            }
        );
    }

    @Test
    @SuppressWarnings("resource")
    public void execute_firstCall_callContextConnect() throws PortalException {
        int opcode = 5;
        FList inputFlist = mock(FList.class);

        assertAll(
            () -> {
                PortalContext mockContext = mock(PortalContext.class);
                PcmConnection testConnection = new PcmConnection(mockContext);

                testConnection.execute(opcode, inputFlist);

                verify(mockContext).connect();
            },
            () -> {
                PortalContext mockContext = mock(PortalContext.class);
                PcmConnection testConnection = new PcmConnection(mockContext);

                int flags = 7;
                testConnection.execute(opcode, flags, inputFlist);

                verify(mockContext).connect();
            }
        );
    }

    @Test
    @SuppressWarnings("resource")
    public void execute_subsequentCalls_doNotCallContextConnect() throws PortalException {
        int opcode = 5;
        FList inputFlist = mock(FList.class);

        assertAll(
            () -> {
                PortalContext mockContext = mock(PortalContext.class);
                PcmConnection testConnection = new PcmConnection(mockContext);

                testConnection.execute(opcode, inputFlist);
                testConnection.execute(opcode, inputFlist);

                verify(mockContext).connect();
            },
            () -> {
                PortalContext mockContext = mock(PortalContext.class);
                PcmConnection testConnection = new PcmConnection(mockContext);

                int flags = 7;
                testConnection.execute(opcode, flags, inputFlist);
                testConnection.execute(opcode, flags, inputFlist);

                verify(mockContext).connect();
            }
        );
    }

    @Test
    @SuppressWarnings("resource")
    public void transact_nullHandler_throwIllegalArgument() {
        PortalContext mockContext = mock(PortalContext.class);
        PcmConnection testConnection = new PcmConnection(mockContext);

        assertAll(
            () -> assertThrows(IllegalArgumentException.class, () -> testConnection.transact(null)),
            () -> assertThrows(IllegalArgumentException.class, () -> testConnection.transact(0, null))
        );
    }

    @Test
    @SuppressWarnings("resource")
    public void transact_openTransactionBeforeHandlerExecute() {
        assertAll(
            () -> {
                PortalContext mockContext = mock(PortalContext.class);
                PcmConnection testConnection = new PcmConnection(mockContext);

                TransactionHandler mockHandler = mock(TransactionHandler.class);
                testConnection.transact(mockHandler);

                InOrder inOrder = inOrder(mockContext, mockHandler);
                inOrder.verify(mockContext).transactionOpen(anyInt());
                inOrder.verify(mockHandler).execute();
            },
            () -> {
                PortalContext mockContext = mock(PortalContext.class);
                PcmConnection testConnection = new PcmConnection(mockContext);

                int flags = 10;
                TransactionHandler mockHandler = mock(TransactionHandler.class);
                testConnection.transact(flags, mockHandler);

                InOrder inOrder = inOrder(mockContext, mockHandler);
                inOrder.verify(mockContext).transactionOpen(eq(flags));
                inOrder.verify(mockHandler).execute();
            }
        );
    }

    @Test
    @SuppressWarnings("resource")
    public void transact_handlerThrowsException_transactionAbort() {
        TransactionHandler throwingHandler = () -> {
            throw new PortalException("test");
        };

        assertAll(
            () -> {
                PortalContext mockContext = mock(PortalContext.class);
                PcmConnection testConnection = new PcmConnection(mockContext);

                assertThrows(PortalException.class, () -> testConnection.transact(throwingHandler));

                verify(mockContext).transactionAbort();
            },
            () -> {
                PortalContext mockContext = mock(PortalContext.class);
                PcmConnection testConnection = new PcmConnection(mockContext);

                int flags = 10;
                assertThrows(PortalException.class, () -> testConnection.transact(flags, throwingHandler));

                verify(mockContext).transactionAbort();
            }
        );
    }

    @Test
    @SuppressWarnings("resource")
    public void transact_abortThrowsException_suppressAndRethrowException() throws PortalException {
        EBufException exceptionDuringAbort = new EBufException(0, new Object());
        PortalContext mockContext = mockContextThrowingOnAbort(exceptionDuringAbort);
        PcmConnection testConnection = new PcmConnection(mockContext);

        TransactionHandler throwingHandler = () -> {
            throw new PortalException("test");
        };

        assertAll(
            () -> {
                PortalException exceptionThrown = assertThrows(PortalException.class, () -> testConnection.transact(throwingHandler));

                assertIterableEquals(Arrays.asList(exceptionDuringAbort), Arrays.asList(exceptionThrown.getSuppressed()));
            },
            () -> {
                int flags = 10;
                PortalException exceptionThrown = assertThrows(PortalException.class, () -> testConnection.transact(flags, throwingHandler));

                assertIterableEquals(Arrays.asList(exceptionDuringAbort), Arrays.asList(exceptionThrown.getSuppressed()));
            }
        );
    }

    private PortalContext mockContextThrowingOnAbort(EBufException exception) throws PortalException {
        PortalContext mockContext = mock(PortalContext.class);
        doThrow(exception).when(mockContext).transactionAbort();
        return mockContext;
    }

    static {
        mock(TransactionHandler.class);
        mock(PortalContext.class);
        mock(FList.class);
    }
}
