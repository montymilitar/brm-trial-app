package com.pccw.brm.client;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.withSettings;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import com.portal.pcm.PortalException;

public class ClientTest {
    @Test
    public void constructor_nullFactory_throwIllegalArgument() {
        assertThrows(IllegalArgumentException.class, () -> new Client(null));
    }

    @Test
    public void connect_nullHandler_throwIllegalArgument() {
        Client testClient = new Client(mock(ConnectionFactory.class));

        assertThrows(IllegalArgumentException.class, () -> testClient.connect(null));
    }

    @Test
    public void connect_factoryReturnedNullConnection_throwIllegalState() {
        Client testClient = new Client(() -> null);

        assertThrows(IllegalStateException.class, () -> testClient.connect(conn -> {}));
    }

    @Test
    public void connect_factoryReturnedConnection_callHandler() throws PortalException {
        Connection mockConnection = mock(Connection.class, withSettings().stubOnly());
        Client testClient = new Client(() -> mockConnection);

        ConnectionHandler mockHandler = mock(ConnectionHandler.class);
        testClient.connect(mockHandler);

        verify(mockHandler).handle(same(mockConnection));
    }

    @Test
    public void connect_handlerDone_closeConnection() throws PortalException {
        Connection mockConnection = mock(Connection.class);
        Client testClient = new Client(() -> mockConnection);

        testClient.connect(connection -> {});

        verify(mockConnection).close();
    }

    @Test
    public void connect_handlerThrewException_closeThrewException_suppressAndThrowExceptions() throws PortalException {
        PortalException exceptionDuringClose = new PortalException("close");
        Client testClient = new Client(() -> mockConnectionToThrowWhenClosing(exceptionDuringClose));

        ConnectionHandler throwingHandler = connection -> {
            throw new PortalException("test");
        };

        PortalException exceptionThrown = assertThrows(PortalException.class, () -> testClient.connect(throwingHandler));
        assertIterableEquals(Arrays.asList(exceptionDuringClose), Arrays.asList(exceptionThrown.getSuppressed()));
    }

    private Connection mockConnectionToThrowWhenClosing(PortalException exceptionToThrow) throws PortalException {
        Connection mockConnection = mock(Connection.class);
        doThrow(exceptionToThrow).when(mockConnection).close();
        return mockConnection;
    }

    static {
        mock(ConnectionFactory.class);
        mock(Connection.class);
        mock(ConnectionHandler.class);
    }
}
