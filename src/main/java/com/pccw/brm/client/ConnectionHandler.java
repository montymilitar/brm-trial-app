package com.pccw.brm.client;

import com.portal.pcm.PortalException;

public interface ConnectionHandler {
    void handle(Connection connection) throws PortalException;
}
