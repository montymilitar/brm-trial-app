package com.pccw.brm.client;

import com.portal.pcm.PortalException;

public interface ConnectionFactory {
    Connection newConnection() throws PortalException;
}
