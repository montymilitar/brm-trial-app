package com.pccw.brm.client.pcm;

import org.apache.log4j.Logger;

import com.pccw.brm.client.Client;
import com.pccw.brm.client.Connection;
import com.pccw.brm.client.TransactionHandler;
import com.portal.pcm.FList;
import com.portal.pcm.PortalContext;
import com.portal.pcm.PortalException;

public final class PcmConnection implements Connection {
    private static final Logger log = Logger.getLogger(Client.class);
    private final PortalContext context;
    private boolean isClosed;
    private boolean isConnected;

    public PcmConnection(PortalContext context) {
        if (context == null)
            throw new IllegalArgumentException("Null portal context");

        this.context = context;
        this.isClosed = false;
        this.isConnected = false;
    }

    @Override
    public FList execute(int opcode, int flags, FList input) throws PortalException {
        connectContext();

        log.debug("Execution parameters: opcode=" + opcode + " flags=" + flags);
        log.debug("Execution input flist: ");
        log.debug(input.asString());

        FList output = context.opcode(opcode, flags, input);
        if (output != null) {
            log.debug("Execution output flist: ");
            log.debug(output.asString());
        }

        return output;
    }

    @Override
    public void transact(int flags, TransactionHandler handler) throws PortalException {
        if (handler == null)
            throw new IllegalArgumentException("Null transaction handler");

        log.debug("Transaction parameters: flags=" + flags);
        log.debug("Opening transaction...");
        context.transactionOpen(flags);
        log.debug("Transaction mode start");
        try {
            log.debug("Calling transaction handler...");
            handler.execute();
            log.debug("Transaction handler done");
        } catch (PortalException handlerException) {
            log.error("Exception in handler");
            try {
                log.debug("Aborting transaction...");
                context.transactionAbort();
                log.debug("Transaction mode end");
            } catch (PortalException abortException) {
                log.error("Exception while aborting");
                handlerException.addSuppressed(abortException);
            }

            log.error("Exception thrown:", handlerException);
            throw handlerException;
        }
        log.debug("Committing transaction...");
        context.transactionCommit();
        log.debug("Transaction mode end");
    }

    @Override
    public void close() throws PortalException {
        if (!isClosed) {
            disconnectContext();
            log.debug("Closing connection...");
            context.close(true);
            isClosed = true;
            log.debug("Connection closed");
        }
    }

    private void connectContext() throws PortalException {
        if (!isConnected) {
            log.debug("Connecting portal context...");
            context.connect();
            isConnected = true;
            log.debug("Portal context connected");
        }
    }

    private void disconnectContext() throws PortalException {
        if (isConnected) {
            log.debug("Disconnecting portal context...");
            context.disconnect();
            isConnected = false;
            log.debug("Portal context disconnected");
        }
    }
}
