package com.pccw.brm.client;

import com.portal.pcm.PortalException;

public interface TransactionHandler {
    void execute() throws PortalException;
}
