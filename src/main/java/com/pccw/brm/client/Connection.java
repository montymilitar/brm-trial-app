package com.pccw.brm.client;

import com.portal.pcm.FList;
import com.portal.pcm.PortalException;

public interface Connection extends AutoCloseable {
    FList execute(int opcode, int flags, FList input) throws PortalException;

    default FList execute(int opcode, FList input) throws PortalException {
        return execute(opcode, 0, input);
    }

    void transact(int flags, TransactionHandler handler) throws PortalException;

    default void transact(TransactionHandler handler) throws PortalException {
        transact(0, handler);
    }

    @Override
    void close() throws PortalException;
}
