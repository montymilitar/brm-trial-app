package com.pccw.brm.client;

import org.apache.log4j.Logger;

import com.portal.pcm.PortalException;

public final class Client {
    private static final Logger log = Logger.getLogger(Client.class);
    private final ConnectionFactory factory;

    public Client(ConnectionFactory factory) {
        if (factory == null)
            throw new IllegalArgumentException("Null connection factory");

        this.factory = factory;
    }

    public void connect(ConnectionHandler handler) throws PortalException {
        if (handler == null)
            throw new IllegalArgumentException("Null connection handler");

        log.debug("Connecting to BRM...");
        try (Connection connection = newConnectionFromFactory()) {
            log.debug("Connection established");
            log.debug("Calling connection handler...");
            handler.handle(connection);
            log.debug("Connection handler done");
        }
    }

    private Connection newConnectionFromFactory() throws PortalException {
        Connection newConnection = factory.newConnection();
        if (newConnection == null)
            throw new IllegalStateException("No connection provided");

        return newConnection;
    }
}
