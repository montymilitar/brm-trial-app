package com.pccw.brm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Paths;
import java.util.Properties;

import com.pccw.brm.client.Client;
import com.pccw.brm.client.pcm.PcmConnection;
import com.portal.pcm.FList;
import com.portal.pcm.Poid;
import com.portal.pcm.PortalContext;
import com.portal.pcm.PortalOp;
import com.portal.pcm.fields.FldFirstName;
import com.portal.pcm.fields.FldLastName;
import com.portal.pcm.fields.FldPoid;

public class SampleClient {
    public static void main(String[] args) throws Exception {
        // get properties file
        File propertiesFile = getPropertiesFile();

        // load properties from file
        Properties properties = loadProperties(propertiesFile);

        // initialize client from properties
        Client client = new Client(() -> new PcmConnection(new PortalContext(properties)));

        client.connect(connection -> {
            System.out.println("Doing test loopback...");

            FList input = new FList();
            input.set(FldPoid.getInst(), new Poid(1));
            input.set(FldFirstName.getInst(), "John");
            input.set(FldLastName.getInst(), "Doe");

            System.out.println("Input Flist:");
            input.dump();

            System.out.println("Executing test loopback...");
            FList output = connection.execute(PortalOp.TEST_LOOPBACK, input);
            System.out.println("Execution done");

            System.out.println("Output Flist:");
            output.dump();

            System.out.println("Transacting...");
            connection.transact(() -> {
                System.out.println("Doing test loopback 1 in transaction...");

                FList txnInput1 = new FList();
                txnInput1.set(FldPoid.getInst(), new Poid(1));
                txnInput1.set(FldFirstName.getInst(), "Test 1");
                txnInput1.set(FldLastName.getInst(), "Test 1");

                System.out.println("Input Flist:");
                txnInput1.dump();

                System.out.println("Executing test loopback 1 in transaction...");
                FList txnOutput1 = connection.execute(PortalOp.TEST_LOOPBACK, txnInput1);
                System.out.println("Execution in transaction done");

                System.out.println("Output Flist:");
                txnOutput1.dump();

                System.out.println("Doing test loopback 2 in transaction...");

                FList txnInput2 = new FList();
                txnInput2.set(FldPoid.getInst(), new Poid(1));
                txnInput2.set(FldFirstName.getInst(), "Test 2");
                txnInput2.set(FldLastName.getInst(), "Test 2");

                System.out.println("Input Flist:");
                txnInput2.dump();

                System.out.println("Executing test loopback 2 in transaction...");
                FList txnOutput2 = connection.execute(PortalOp.TEST_LOOPBACK, txnInput2);
                System.out.println("Execution in transaction done");

                System.out.println("Output Flist:");
                txnOutput2.dump();
            });
            System.out.println("Transaction done");
        });
    }

    private static File getPropertiesFile() {
        // -D argument check: infranet.properties.path
        String pathValue = System.getProperty("infranet.properties.path", "");
        if (pathValue.isEmpty())
            throw new IllegalArgumentException("No infranet.properties.path set; use -Dinfranet.properties.path=<full path> when calling this application");

        File file = Paths.get(pathValue).toAbsolutePath().toFile();
        if (!file.exists())
            throw new IllegalArgumentException(file.getAbsolutePath() + " is missing");
        if (!file.canRead())
            throw new IllegalArgumentException(file.getAbsolutePath() + " is not readable");

        return file;
    }

    private static Properties loadProperties(File file) throws IOException {
        Properties properties = new Properties();
        try (Reader fileReader = new BufferedReader(new FileReader(file))) {
            properties.load(fileReader);
        }
        return properties;
    }
}
